/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.Entite;

import java.util.Date;

/**
 *
 * @author MBM info
 */
public class produit {
    public int id_produit;
    public int categorie_id;
     public int boutique_id;
      public int promotion;
       public String titre;
        public double prix;
         public String description;
          public boolean stock_p;
           public int rating;
            public int quantité;
             public Date datecreation;
              public double prixprom;

    public produit() {
    }

    public produit(int id_produit, int categorie_id, int boutique_id, int promotion, String titre, double prix, String description, boolean stock_p, int rating, int quantité, Date datecreation, double prixprom) {
        this.id_produit = id_produit;
        this.categorie_id = categorie_id;
        this.boutique_id = boutique_id;
        this.promotion = promotion;
        this.titre = titre;
        this.prix = prix;
        this.description = description;
        this.stock_p = stock_p;
        this.rating = rating;
        this.quantité = quantité;
        this.datecreation = datecreation;
        this.prixprom = prixprom;
    }
    

    @Override
    public String toString() {
        return "produit{" + "id_produit=" + id_produit + ", categorie_id=" + categorie_id + ", boutique_id=" + boutique_id + ", promotion=" + promotion + ", titre=" + titre + ", prix=" + prix + ", description=" + description + ", stock_p=" + stock_p + ", rating=" + rating + ", quantit\u00e9=" + quantité + ", datecreation=" + datecreation + ", prixprom=" + prixprom + '}';
    }

    public int getId_produit() {
        return id_produit;
    }

    public void setId_produit(int id_produit) {
        this.id_produit = id_produit;
    }

    public int getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(int categorie_id) {
        this.categorie_id = categorie_id;
    }

    public int getBoutique_id() {
        return boutique_id;
    }

    public void setBoutique_id(int boutique_id) {
        this.boutique_id = boutique_id;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStock_p() {
        return stock_p;
    }

    public void setStock_p(boolean stock_p) {
        this.stock_p = stock_p;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getQuantité() {
        return quantité;
    }

    public void setQuantité(int quantité) {
        this.quantité = quantité;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public double getPrixprom() {
        return prixprom;
    }

    public void setPrixprom(double prixprom) {
        this.prixprom = prixprom;
    }
}
