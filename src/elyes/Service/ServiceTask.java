/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.Service;

import com.codename1.components.ImageViewer;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.File;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.util.ImageIO;
import com.codename1.util.Base64;
import elyes.Entite.artisan;

import elyes.Entite.boutique;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sana
 */
public class ServiceTask {
String str ;
    public void ajoutTask(boutique b) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/VERSION%20FINAAL/PIDEV2/web/app_dev.php/bt/new?nom_bout=" + b.getNombout() + "&adresse=" + b.getAdresse()+ "&numtel=" +b.getNumtel()+ "&image=" +b.getImage()+"&user_id="+b.getUserid();
        con.setUrl(Url);

        //System.out.println("tt");

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public ArrayList<boutique> getListTask(String json) {

        ArrayList<boutique> btou = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();

            Map<String, Object> btmap = j.parseJSON(new CharArrayReader(json.toCharArray()));
           
            List<Map<String, Object>> list = (List<Map<String, Object>>) btmap.get("root");
Map<String, Object> list2 ;
            for (Map<String, Object> obj : list) {
                list2=(Map<String, Object>) obj.get("user");
                boutique b=new boutique();

                float id = Float.parseFloat(obj.get("id").toString());
                b.setId((int) id);
                b.setAdresse(obj.get("adresse").toString());
               b.setNombout(obj.get("nombout").toString());
              b.setUserid((int)Float.parseFloat(list2.get("id").toString()));
float idd=Float.parseFloat(obj.get("numtel").toString());
               b.setNumtel(String.valueOf((int)idd));
        b.setImage(obj.get("image").toString());
                btou.add(b);

            }

        } catch (IOException ex) {
        }
        return btou;

    }
    //java.util.List<Map<String, Object>> content = (java.util.List<Map<String, Object>>)data.get("root");
    /*
    message = new String(cn.getResponseData(), "utf-8");
                       JSONParser parser = new JSONParser();
                       try {
                           Map<String,Object> obj = parser.parseJSON(new InputStreamReader(new ByteArrayInputStream(cn.getResponseData()), "utf-8"));
    */
    
    ArrayList<boutique> listTasks = new ArrayList<>();
    
    public ArrayList<boutique> getList2(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/VERSION%20FINAAL/PIDEV2/web/app_dev.php/bt/all");  
        con.addResponseListener((NetworkEvent evt) -> {
            ServiceTask ser = new ServiceTask();
            JSONParser parser = new JSONParser();
            try {
                Map<String,Object> data = parser.parseJSON(new InputStreamReader(Display.getInstance().getResourceAsStream(getClass(), "/file.json"),"utf-8"));

            
            } catch (Exception e) {
            }
            listTasks = ser.getListTask(new String(con.getResponseData()));
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTasks;
    }

      public Boolean estAbonTask(int idu,int idb) {
          
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/VERSION%20FINAAL/PIDEV2/web/app_dev.php/bt/isAb?idu="+idu+"&idb="+idb;
        con.setUrl(Url);

        //System.out.println("tt");

        con.addResponseListener((e) -> {
             str = new String(con.getResponseData());

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return Boolean.valueOf(str);
    }

    public void abonneTask(int idu ,int idb) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/VERSION%20FINAAL/PIDEV2/web/app_dev.php/bt/Ab?idu="+idu+"&idb="+idb;
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
        public void deabonneTask(int idu ,int idb) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/VERSION%20FINAAL/PIDEV2/web/app_dev.php/bt/desAb?idu="+idu+"&idb="+idb;
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
     public ArrayList<artisan> getListart(String json) {

        ArrayList<artisan> listart = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();

            Map<String, Object> artis = j.parseJSON(new CharArrayReader(json.toCharArray()));
           
            List<Map<String, Object>> list = (List<Map<String, Object>>) artis.get("root");

            for (Map<String, Object> obj : list) {
                artisan a=new artisan();

                float id = Float.parseFloat(obj.get("id").toString());
                a.setId((int) id);
//                  System.out.println(obj.get("user_id"));
//                float iduser = Float.parseFloat(obj.get("id").toString());
//                System.out.println(id);
//                b.setUserid((int) iduser);
                //e.setId(Integer.parseInt(obj.get("id").toString().trim()));
               // e.setEtat(obj.get("state").toString());
                a.setAdresse(obj.get("adresse").toString());
                a.setNomart(obj.get("nomart").toString());
//                b.setAdresse(obj.get("adresse").toString());
               a.setNumtel(obj.get("numtel").toString());
        a.setImage(obj.get("image").toString());
                //e.setNom(obj.get("name").toString());
                //System.out.println(e);
                listart.add(a);

            }

        } catch (IOException ex) {
        }
        return listart;

    }
    
    
    ArrayList<artisan> listartisan = new ArrayList<>();
    
    public ArrayList<artisan> getLisart2(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/VERSION%20FINAAL/PIDEV2/web/app_dev.php/bt/allart");  
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceTask ser = new ServiceTask();
                listartisan = ser.getListart(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listartisan;
    }
    
    
     public void supprimerboutique(int id) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/VERSION FINAAL/PIDEV2/web/app_dev.php/sup?id="+id;
        con.setUrl(Url);

        System.out.println("t");

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
//            if (str.trim().equalsIgnoreCase("OK")) {
//                f2.setTitle(tlogin.getText());
//             f2.show();
//            }
//            else{
//            Dialog.show("error", "login ou pwd invalid", "ok", null);
//            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
      public void modifierboutique(boutique b) {
        ConnectionRequest con = new ConnectionRequest();
      String Url=  "http://localhost/VERSION FINAAL/PIDEV2/web/app_dev.php/up?id="+b.getId() +"&nom_bout="+b.getNombout() +"&adresse="+b.getAdresse() +"&numtel="+b.getNumtel() +"&image="+b.getImage();
        con.setUrl(Url);

        System.out.println("tt");

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
//            if (str.trim().equalsIgnoreCase("OK")) {
//                f2.setTitle(tlogin.getText());
//             f2.show();
//            }
//            else{
//            Dialog.show("error", "login ou pwd invalid", "ok", null);
//            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        
    }
      
   public void setImage(String filePath, ImageViewer iv) {
           try {
               //creation d'image apartir du filepath
               Image i1 = Image.createImage(filePath).scaled(400,400);
               iv.setImage(i1);
               if (i1 != null) {
                   //FileSystemStorage  
                   //trodek tnajm testoki l image en binaire
                   ImageIO imgIO = ImageIO.getImageIO();
                   //stocker l'inage dans le flux
                   ByteArrayOutputStream out = new ByteArrayOutputStream();
                   imgIO.save(i1, out, File.separator, 1);
                   //recuperer l image du flux dans un tab binaire
                   byte[] ba = out.toByteArray();
                   //cryptage de l image binaire
                   String Imagecode = Base64.encode(ba);
                   ConnectionRequest request = new ConnectionRequest();
                   request.addResponseListener((NetworkEvent evt) -> {
                       byte[] data = (byte[]) evt.getMetaData();
                       String imageName = new String(data);
                       System.out.println("metadata "+imageName);
                       iv.getImage().setImageName(imageName);
                   });
                   request.setPost(true);
                   request.setHttpMethod("POST");
                  // imagecode sequence binaire de l image coder
                   request.addArgument("Image", Imagecode);
                   request.setUrl("http://localhost:80/Upload.php");
                   NetworkManager.getInstance().addToQueueAndWait(request);
               } else {
                   System.out.println("Unable to upload");
               }
               iv.getParent().revalidate();

           } catch (Exception ex) {
            
           }
       
    }
        
 public void browseImage(ImageViewer im){
     //open gallery
     Display.getInstance().openGallery((ActionListener) (ActionEvent ev) -> {
         
         if (ev != null && ev.getSource() != null) {
             String filePath = (String) ev.getSource();
                // retenue de nom d'image
//             int fileNameIndex = filePath.lastIndexOf("/") + 1;
//             String fileName = filePath.substring(fileNameIndex);
             // Do something
             
             setImage(filePath,im);
         }
     }, Display.GALLERY_IMAGE);
    
 }    

}
