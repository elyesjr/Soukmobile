/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import elyes.Entite.boutique;
import elyes.Service.ServiceTask;
import java.io.IOException;

/**
 *
 * @author MBM info
 */
public class detailBoutique {
    
    Form f ;
  private ImageViewer imgv;
    private Image img;
    private EncodedImage enc;
    Button bsupp;
    Button bmodif;
    Button ab;
    public  detailBoutique(boutique b) throws IOException{
        ServiceTask ser=new ServiceTask();
        enc = EncodedImage.create("/giphy.gif");
        f=new Form("Dètail boutique");
        Container c=new Container(BoxLayout.y());
        Container c1=new Container(BoxLayout.x());
                Container c2=new Container(BoxLayout.x());
        Container c3=new Container(BoxLayout.x());
        Container c4=new Container(BoxLayout.x());
        
        Label l1=new Label("nom :");
        Label l2=new Label(b.getNombout());
        Label l3=new Label("adresse :");
        Label l4=new Label(b.getAdresse());
        Label l5=new Label("num Tel :");
        Label l6=new Label(b.getNumtel());
        ab=new Button();
        bsupp=new Button("supprimer boutique");
        bmodif=new Button("modifier boutique");
        img = URLImage.createToStorage(enc, "image"+b.getImage(), "http://localhost/VERSION%20FINAAL/PIDEV2/web/boutique_image/"+b.getImage(), URLImage.RESIZE_SCALE_TO_FILL);
                        imgv = new ImageViewer(img.scaled(120, 150));
                        System.out.println("id boutique="+b.getId());
      if(ser.estAbonTask(login.userId, b.getId())==true){
          ab.setText("Annuler abonnement");
      }else{
            ab.setText("Abonner");
      }
      ab.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if(ab.getText()=="Abonner"){
                              ab.setText("desabonner");
                              ser.abonneTask(login.userId, b.getId());

                }else{
                                ab.setText("Abonner");
                                ser.deabonneTask(login.userId, b.getId());

                }
            }
        });
       bsupp.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                          
                            ser.supprimerboutique(b.getId());
                           AffichageB a=new AffichageB();
                            a.getF().show();
                        }
                    });
       
        bmodif.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                         
                         
                            Modificationboutique m = new Modificationboutique(b);
                            m.getAjoutForm().show();
                        }
                    });
        c1.add(l1);
        c1.add(l2);
     //   c.add(imgv);
        c2.add(l3);
        c2.add(l4);
        c3.add(l5);
        c3.add(l6);
     
        c.add(c1);
     c.add(c2);
     c.add(c3);
     if(userOwn(b))
     {
         c.add(bsupp);
         c.add(bmodif);
     }
     else
     {
         c.add(ab);
     }
       c4.add(imgv);  
       c4.add(c);
    
         f.getToolbar().addCommandToRightBar("back", null, (ev)->{AffichageB hh;
         hh = new AffichageB();
         hh.getF().show();
         f.refreshTheme();
         
          });
         f.add(c4);
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    public boolean userOwn(boutique b){
        System.out.println("user login id = "+login.userId);
        System.out.println("user boutique id = "+b.getUserid());
        return b.getUserid() == login.userId;
    }
    
}
