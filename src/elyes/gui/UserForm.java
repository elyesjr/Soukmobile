package elyes.gui;

import com.codename1.components.ScaleImageLabel;
import com.codename1.facebook.FaceBookAccess;
import com.codename1.facebook.User;
import com.codename1.io.Storage;
import com.codename1.social.FacebookConnect;
import com.codename1.social.Login;
import com.codename1.social.LoginCallback;
import com.codename1.ui.Button;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.io.IOException;

public class UserForm  {
    Form f;
    public UserForm() {
         f=new Form();
         String clientId = "2052815018079124";
    String redirectURI = "https://www.youtube.com/";
    String clientSecret = "f37aca5a10e06e5ecd02289f68f015f2";
    Login fb = FacebookConnect.getInstance();

    fb.setClientId(clientId);

    fb.setRedirectURI(redirectURI);

    fb.setClientSecret (clientSecret);
    //Sets a LoginCallback listener
    //  fb.setCallback();
    //trigger the login if not already logged in    //Sets a LoginCallback listener
    //  fb.setCallback();
    //trigger the login if not already logged in
  fb.setCallback(new LoginCallback() {
            
            @Override
            
            public void loginFailed(String errorMessage) {
 
                System.out.println("Falló el login");
                Storage.getInstance().writeObject("token", "EAAdLBj3xM5QBAJanWAb89IOOUC2F2IpT3szIwwJLRuJQjNsgTf4ytpzHbaPBpQfQc2dNmL8t2XJE8GOwjomZBwBXOw1offrHyPtJ9LNfXbSlUI6o4M1ud4WZC9wOIWV8IItjZCTpnJZAX7f8MARZCLUXZCRoSCuZBvRt4EgM4xqDamsVw02Koqc76mV4KHXBqwI6ZCeTXi8xKgZDZD");
               
            }

            @Override
            public void loginSuccessful() {
                System.out.println("Funcionó el login");
                String token = fb.getAccessToken().getToken();
                Storage.getInstance().writeObject("token", token);
               
               Acceuil a=new Acceuil();
               a.getF().show();
            }
            
        });
    if(!fb.isUserLoggedIn () 
        ){
       
                    fb.doLogin();
            /*       */
    }

    
    
        else{
                    //get the token and now you can query the facebook API
              //      String token = fb.getAccessToken().getToken();
                 //     Storage.getInstance().writeObject("token", token);
                        String token = (String) Storage.getInstance().readObject("token");
        FaceBookAccess.setToken(token);
            final User me = new User();
            try {
                FaceBookAccess.getInstance().getUser("me", me, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        String miNombre = me.getName();
                        
                        f.getContentPane().removeAll();
                        
                        f.add(new Label(miNombre));
                        
                        Button buttonLogout = new Button("Logout");
                        buttonLogout.addActionListener((e) -> {
                            fb.doLogout();
                            //showIfNotLoggedIn(form);
                           
                        });

                        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(50, 50, 0xffff0000), true);
                        URLImage background = URLImage.createToStorage(placeholder, "fbuser.jpg",
                                "https://graph.facebook.com/v2.11/me/picture?access_token=" + token);
                        background.fetch();
                        ScaleImageLabel myPic = new ScaleImageLabel();
                        myPic.setIcon(background);
                        
                        f.add(myPic);
                        f.add(buttonLogout);
                        
                        f.revalidate();
                        //form.show();
                    }

                    
                });
            } catch (IOException ex) {
                ex.printStackTrace();
               // showIfNotLoggedIn(form);
                System.out.println("erreur");
            }
               Acceuil a=new Acceuil();
               a.getF().show();

    }
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

   

}
