/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.validation.Constraint;
import com.codename1.ui.validation.Validator;
import elyes.Service.ServiceTask;
import elyes.Entite.boutique;
import java.io.IOException;



/**
 *
 * @author sana
 */
public class HomeForm {

    Form f;
    TextField tnom;
    TextField tadr;
        TextField tnumtel;
    ImageViewer im;
    Button btnajout,btnaff,choose;

    public HomeForm() {
        f = new Form("home");
        tnom = new TextField("","nom",20,TextArea.ANY);
        tadr = new TextField("","adresse",20,TextArea.ANY);
        tnumtel = new TextField("","numero de telephone",20,TextArea.PHONENUMBER);
        im=new ImageViewer();
        im.setSize(new Dimension(300, 200));
        choose=new Button("choose image");
        btnajout = new Button("ajouter");
        btnaff=new Button("Affichage");
        
        Validator validator = new Validator();
        validator.addSubmitButtons(btnajout);
        
        validator.addConstraint(tnom, new Constraint() {
            @Override
            public boolean isValid(Object value) {
               return !String.valueOf(value).equals("");
            }

            @Override
            public String getDefaultFailMessage() {
                return "Champ nom vide";
            }
        });
        validator.addConstraint(tadr, new Constraint() {
            @Override
            public boolean isValid(Object value) {
                return !String.valueOf(value).equals("");
            }

            @Override
            public String getDefaultFailMessage() {
                return "Champ adresse vide";
            }
        });
        validator.addConstraint(tnumtel, new Constraint() {
            @Override
            public boolean isValid(Object value) {
                return String.valueOf(value).length() == 8;
            }

            @Override
            public String getDefaultFailMessage() {
               return  "num tel -8 chiffres";
            }
        });
        
     //   System.out.println(validator.getErrorMessage(tnumtel));
        
        
        
 
        f.add(tnom);
        f.add(tadr);
        f.add(tnumtel);
       
       f.add(choose);
        f.add(btnajout);
        f.add(btnaff);
        f.add(im);
        
        choose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
           ServiceTask s=new ServiceTask();
           s.browseImage(im);
          
            }
        });
        btnajout.addActionListener((e) -> {
            ServiceTask ser = new ServiceTask();
            System.out.println(im.getImage().getImageName());
//            if (logfb.fbUser!=null) {
//                 boutique b = new boutique(tadr.getText(),tnumtel.getText(), tnom.getText(),im.getImage().getImageName(),logfb.fbUser.getId());
//            ser.ajoutTask(b);
//            }else{
     boutique b = new boutique(tadr.getText(),tnumtel.getText(), tnom.getText(),im.getImage().getImageName(),login.userId);

           ser.ajoutTask(b);
//        }
            System.out.println("boutique user id after ajout = "+b.getUserid());

        });
        btnaff.addActionListener((e)->{
        AffichageB a;
        a = new AffichageB();
        a.getF().show();
        
        });
        
        btnaff.refreshTheme();
    }

    public ImageViewer getIm() {
        return im;
    }

    public void setIm(ImageViewer im) {
        this.im = im;
    }

    public Button getBtnajout() {
        return btnajout;
    }

    public void setBtnajout(Button btnajout) {
        this.btnajout = btnajout;
    }

    public Button getBtnaff() {
        return btnaff;
    }

    public void setBtnaff(Button btnaff) {
        this.btnaff = btnaff;
    }

    public Button getChoose() {
        return choose;
    }

    public void setChoose(Button choose) {
        this.choose = choose;
    }

    public TextField getTadr() {
        return tadr;
    }

    public void setTadr(TextField tadr) {
        this.tadr = tadr;
    }

    public TextField getTnumtel() {
        return tnumtel;
    }

    public void setTnumtel(TextField tnumtel) {
        this.tnumtel = tnumtel;
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public TextField getTnom() {
        return tnom;
    }

    public void setTnom(TextField tnom) {
        this.tnom = tnom;
    }

}
