/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import elyes.Entite.artisan;
import elyes.Entite.boutique;
import elyes.Service.ServiceTask;
import java.io.IOException;
import javafx.scene.transform.Transform;

/**
 *
 * @author MBM info
 */
public class detailArtisan {
    Form f ;
  private ImageViewer imgv;
    private Image img;
    private EncodedImage enc;
    public  detailArtisan(artisan a) throws IOException{
        ServiceTask ser=new ServiceTask();
        enc = EncodedImage.create("/giphy.gif");
        f=new Form("Dètail artisan");
        Container c=new Container(BoxLayout.y());
        Container c1=new Container(BoxLayout.x());
                Container c2=new Container(BoxLayout.x());
        Container c3=new Container(BoxLayout.x());
        Container c4=new Container(BoxLayout.x());

        Label l1=new Label("nom :");
        Label l2=new Label(a.getNomart());
        Label l3=new Label("adresse :");
        Label l4=new Label(a.getAdresse());
        Label l5=new Label("num Tel :");
        Label l6=new Label(a.getNumtel());

        img = URLImage.createToStorage(enc, "image"+a.getImage(), "http://localhost/VERSION%20FINAAL/PIDEV2/web/artisan_image/"+a.getImage(), URLImage.RESIZE_SCALE_TO_FILL);
                        imgv = new ImageViewer(img.scaled(100, 100));
                        System.out.println("id artisan="+a.getId());
    
            
        
        c1.add(l1);
        c1.add(l2);
       // c.add(imgv);
        c2.add(l3);
        c2.add(l4);
        c3.add(l5);
        c3.add(l6);
     c.add(c1);
     c.add(c2);
     c.add(c3);
     c4.add(c);
     c4.add(imgv);
         f.getToolbar().addCommandToRightBar("back", null, (ev)->{afficheartisan hh;
         hh = new afficheartisan();
         hh.getF().show();
         
          });
         f.add(c4);
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

  
    
}
