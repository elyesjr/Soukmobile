/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.validation.Constraint;
import com.codename1.ui.validation.Validator;
import elyes.Entite.boutique;
import elyes.Service.ServiceTask;

/**
 *
 * @author MBM info
 */
public class Modificationboutique {
     Form ajoutForm;

  
    
    
    TextField nom;
    TextField adresse;
    TextField numtel;
   
     Button bajout;
     ImageViewer im;
    Button choose;
     public Modificationboutique(boutique b){
    
    ajoutForm=new Form("modifier boutique");
         bajout=new Button("modifier boutique");
        nom=new TextField();
        adresse=new TextField();
         numtel=new TextField();
          im=new ImageViewer();
choose=new Button("choose image");
       
        nom.setText(b.getNombout());
        adresse.setText(b.getAdresse());
        numtel.setText(b.getNumtel());
        Validator validator = new Validator();
        validator.addSubmitButtons(bajout);
        validator.addConstraint(nom, new Constraint() {
            @Override
            public boolean isValid(Object value) {
               return !String.valueOf(value).equals("");
            }

            @Override
            public String getDefaultFailMessage() {
                return "Champ numtel vide";
            }
        });
         validator.addConstraint(adresse, new Constraint() {
            @Override
            public boolean isValid(Object value) {
               return !String.valueOf(value).equals("");
            }

            @Override
            public String getDefaultFailMessage() {
                return "Champ adresse vide";
            }
        });
         
        validator.addConstraint(numtel, new Constraint() {
            @Override
            public boolean isValid(Object value) {
                return String.valueOf(value).length() == 8;
            }

            @Override
            public String getDefaultFailMessage() {
               return  "num tel -8 chiffres";
            }
        });
        
         choose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
           ServiceTask s=new ServiceTask();
           s.browseImage(im);
          
            }
        });
        bajout.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent evt) {
                 boutique bb = new boutique();
                 bb.setNombout(nom.getText());
                 bb.setAdresse(adresse.getText());
                 bb.setNumtel(numtel.getText());
                 
                 bb.setImage(im.getImage().getImageName());
               //  b.setUserid(Login.user.getId());
                 System.out.println(b.getId());
               bb.setUserid(2);
               bb.setId(b.getId());
               
                  ServiceTask s = new ServiceTask();
                 s.modifierboutique(bb);
                 AffichageB l=new AffichageB();
                 l.getF().show();
             }

      
         });
        
        
        
         ajoutForm.add(nom);
        ajoutForm.add(adresse);
        ajoutForm.add(numtel);
         ajoutForm.add(im);
      ajoutForm.add(choose);

        ajoutForm.add(bajout);
        
        bajout.refreshTheme();
        
    }

    public Form getAjoutForm() {
        return ajoutForm;
    }

    public void setAjoutForm(Form ajoutForm) {
        this.ajoutForm = ajoutForm;
    }

}
