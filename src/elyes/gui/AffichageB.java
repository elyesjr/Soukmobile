/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elyes.gui;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import elyes.Entite.boutique;
import elyes.Service.ServiceTask;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author sana
 */
public class AffichageB {

    Form f;
  private ImageViewer imgv;
    private Image img;
    private EncodedImage enc;
    public AffichageB()  {
        
        f = new Form("Liste boutique");
        Container c=new Container(BoxLayout.y());
              // Container c1=new Container(BoxLayout.y());
      //  Container c2=new Container(BoxLayout.y());

    
        ServiceTask serviceTask=new ServiceTask();
      ArrayList<boutique> listTb =serviceTask.getList2();
        try {
            enc = EncodedImage.create("/giphy.gif");
        } catch (IOException ex) {
        }
      for(int i=0 ;i<listTb.size();i++)
      {
          boutique b=new boutique();
          img = URLImage.createToStorage(enc, "image"+listTb.get(i).getImage(), "http://localhost/VERSION%20FINAAL/PIDEV2/web/boutique_image/"+listTb.get(i).getImage(), URLImage.RESIZE_SCALE_TO_FILL);
                        imgv = new ImageViewer(img.scaled(150,150));
          Label l=new Label();
          l.setText(listTb.get(i).getNombout());
          b.setNombout(listTb.get(i).getNombout());
          b.setAdresse(listTb.get(i).getAdresse());
          b.setNumtel(listTb.get(i).getNumtel());
          b.setId(listTb.get(i).getId());
          b.setImage(listTb.get(i).getImage());
          b.setUserid(listTb.get(i).getUserid());
          System.out.println("test 1 user id btq = "+listTb.get(i).getUserid());
          l.addPointerPressedListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent evt) {
                  
                  try {
                     detailBoutique det;
                      det = new detailBoutique(b);
                      
                         det.getF().show();
                  } catch (IOException ex) {
                  }
                   
              }
          });
          c.add(l);
          c.add(imgv);
      }
       f.getToolbar().addCommandToRightBar("back", null, (ev)->{Acceuil hh;
         hh = new Acceuil();
         hh.getF().show();
         f.refreshTheme();
         
          });
       f.getToolbar().addCommandToLeftBar("ajout", null, (ev)->{HomeForm hh;
         hh = new HomeForm();
         hh.getF().show();
         f.refreshTheme();
         
          });
      f.add(c);
      f.refreshTheme();
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
